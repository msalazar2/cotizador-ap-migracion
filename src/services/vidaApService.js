import axios from 'axios';
import configService from './config';

const apService = axios.create({
  baseURL: configService.vidaApUrl,
});
export default apService;
