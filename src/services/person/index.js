import apService from '../vidaApService';

require('babel-polyfill');

const personService = {};

/**
 * Retorna la data del usuario.
 * GET – /persons/${rut}?token_captcha=${token}
 * @param rut {String}
 * @param token {String}
 * @returns {Promise} {
 *      @param foreign {String}
 *      @param fullName {String}
 *      @param lastName {String}
 *      @param names {String}
 *      @param personStatusFine {Boolean}
 * }
 */
personService.getPerson = async (rut, token) => {
  const res = await apService.get(`/persons/${rut}?token_captcha=${token}`);
  return res;
};

export default personService;
