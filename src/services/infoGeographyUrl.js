import axios from 'axios';
import configService from './config';

const simuladorService = axios.create({
    baseURL: 'http://localhost:8080/'
 //   baseURL: configService.infoGeograficaUrl,
});
export default simuladorService;
