import axios from 'axios';
import configService from './config';

const vidaAhorroService = axios.create({
  baseURL: configService.vidaAhorroUrl,
});
export default vidaAhorroService;
