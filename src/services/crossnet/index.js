import apService from '../vidaApService';

require('babel-polyfill');

const crossnetService = {};

crossnetService.sendContactInformation = async function sendContactInformation(payload) {
  const res = await apService.post('/crossnet/contact-information', payload);
  return res;
};

crossnetService.sendContractSigned = async function sendContractSigned(payload) {
  const res = await apService.post('/crossnet/contract-signed', payload);
  return res;
};

export default crossnetService;
