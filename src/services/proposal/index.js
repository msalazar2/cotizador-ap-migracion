import vidaApService from '../vidaApService';

require('babel-polyfill');

const proposalService = {};

proposalService.sendProposal = async function sendProposal(payload) {
  const res = vidaApService.post('/proposal', payload);
  return res;
};

export default proposalService;
