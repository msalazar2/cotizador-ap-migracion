import personCommonService from '../personCommonService';

require('babel-polyfill');

const commonService = {};

commonService.getNationalities = async () => {
  const res = await personCommonService.get('/nationality');
  return res;
};

commonService.getProfessions = async () => {
  try {
    const res = await personCommonService.get('/profession');
    return res.data.data.listado;
  } catch (res) {
    return Promise.reject(res);
  }
};

export default commonService;
