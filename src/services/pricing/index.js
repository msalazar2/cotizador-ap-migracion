import apService from '../vidaApService';

require('babel-polyfill');

const pricingService = {};

pricingService.getPricing = async function getPricing() {
  const res = await apService.get('/pricing');
  return res;
};

export default pricingService;
