import axios from 'axios';
import configService from './config';

const personCommonService = axios.create({
  baseURL: configService.personCommon,
});
export default personCommonService;
