import indicadoresServiceUrl from '../indicadoresServiceUrl';

require('babel-polyfill');

const indicatorsService = {};
indicatorsService.getIndicators = async function getIndicators() {
  const res = await indicadoresServiceUrl.get('/informacion_general/indicadores_economicos');
  return res;
};

export default indicatorsService;
