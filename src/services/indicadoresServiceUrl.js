import axios from 'axios';
import configService from './config';

const indicadoresServiceUrl = axios.create({
  baseURL: configService.infoIndicadoresUrl,
});

export default indicadoresServiceUrl;
