import infoGeographySevice from '../infoGeographyUrl';

require('babel-polyfill');

const infoGeography = {};

infoGeography.getRegion = async function getRegion() {
  const res = infoGeographySevice.get('/region');
  return res;
};
infoGeography.getCity = async function getCity(id) {
  const res = infoGeographySevice.get(`/ciudad?codigoRegion=${id}`);
  return res;
};
infoGeography.getDistrict = async function getDistrict(id) {
  const res = await infoGeographySevice.get(`/comuna?codigoCiudad=${id}`);
  return res;
};
export default infoGeography;
