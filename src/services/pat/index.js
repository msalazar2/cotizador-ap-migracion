import vidaAhorroService from '../vidaAhorroService';

require('babel-polyfill');

const fallbackBankList = [
  { id: 55, descripcion: 'Banco Consorcio' },
  { id: 1, descripcion: 'Banco Chile - Edwards - Citi' },
  { id: 37, descripcion: 'Banco Santander - Banefe' },
  { id: 51, descripcion: 'Banco Falabella' },
  { id: 16, descripcion: 'Banco Bci - Tbank - Nova' },
  { id: 12, descripcion: 'Banco Estado' },
  { id: 14, descripcion: 'Banco Scotiabank' },
  { id: 39, descripcion: 'Banco Itaú' },
  { id: 504, descripcion: 'Banco Scotiabank Azul (BBVA)' },
  { id: 49, descripcion: 'Banco Security' },
  { id: 27, descripcion: 'Banco Corpbanca' },
  { id: 28, descripcion: 'Banco Bice' },
];

const patRepository = {};

patRepository.getBankList = async () => {
  try {
    const res = await vidaAhorroService.get('/ahorro/vida/pago/pat-bank-list');
    return res.data.data.listado;
  } catch (res) {
    return fallbackBankList;
  }
};

export default patRepository;
