
const {
  infoGeograficaUrl = "https://info-geografica.seguros.cert.digital.consorcio.cl",
  infoIndicadoresUrl  = "https://info-indicadores.seguros.cert.digital.consorcio.cl",
  vidaApUrl = "https://vida-accidentes-personales.seguros.cert.digital.consorcio.cl",
  vidaAhorroUrl = "https://vida-accidentes-personales.seguros.cert.digital.consorcio.cl",
  personCommon = "https://person-common-service.seguros.cert.digital.consorcio.cl/"
} = window.liquid;

const configService = {
  infoGeograficaUrl,
  infoIndicadoresUrl,
  vidaApUrl,
  vidaAhorroUrl,
  personCommon,
};

export default configService;
