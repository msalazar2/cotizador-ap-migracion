export default {
  methods: {
    createDataUrl(file) {
      const byteCharacters = atob(file);
      const byteArrays = [];
      for (let offset = 0; offset < byteCharacters.length; offset += 512) {
        const slice = byteCharacters.slice(offset, offset + 512);

        const byteNumbers = new Array(slice.length);
        for (let i = 0; i < slice.length; i++) {
          byteNumbers[i] = slice.charCodeAt(i);
        }

        const byteArray = new Uint8Array(byteNumbers);
        byteArrays.push(byteArray);
      }

      const blob = new Blob(byteArrays, { type: 'application/pdf' });
      return URL.createObjectURL(blob);
    },
    downloadFiles(originFile, downloadfileName, downloadFileExtension, dataUrl, fileIsOKName = '') {
      if (this.isSafari()) {
        window.open(`data:application/pdf;base64,${originFile}`);
      } else {
        const downloadLink = document.createElement('a');
        downloadLink.target = '_blank';
        downloadLink.download = `${downloadfileName}.${downloadFileExtension}`;

        downloadLink.href = dataUrl;
        document.body.appendChild(downloadLink);
        downloadLink.click();

        document.body.removeChild(downloadLink);
      }
      if (fileIsOKName) {
        this.setFileDownloaded(fileIsOKName);
      }
    },
    setFileDownloaded(name) {
      this.SET_VALUE({ name, value: true });
    },
  },
};
