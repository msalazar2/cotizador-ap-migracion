import { mapState, mapActions, mapGetters, mapMutations } from 'vuex';

export default {
  computed: {
    ...mapState([
      'client',
      'rut',
      'nationality',
      'street',
      'streetNumber',
      'village',
      'region',
      'city',
      'district',
      'contactInfo',
      'datosPago',
      'profession',
      'selectedPlan',
      'primaMensualCLP',
      'primaMensualUF',
      'authorizations',
    ]),
    ...mapGetters(['valorUF']),
  },
  methods: {
    ...mapActions(['SEND_PROPOSAL', 'SEND_CONTRACT_SIGNED']),
    ...mapMutations(['RESET_STATE']),
    setContractSignedPayload() {
      const payload = {
        rut: this.rut,
        telefono: this.contactInfo.telephone.replace(/ /g, ''),
        email: this.contactInfo.email,
        aceptaCondContacto: this.authorizations.aceptaUsoInfoPersonal,
      };
      return payload;
    },
    setProposalPayload() {
      const payload = {
        /* Datos Cliente */
        rut: this.rut,
        nationalityCode:
          this.nationality === 'Chilena' ? 1 : this.nationality.id,
        professionCode: this.profession.id,
        /* Contacto */
        email: this.contactInfo.email,
        mobilePhone: this.contactInfo.telephone.replace(/ /g, ''),
        regionCode: Number(this.region.codRegion),
        cityCode: Number(this.city.codCiudad),
        communeCode: Number(this.district.codComuna),
        streetName: this.street,
        houseNumber: this.streetNumber,
        houseNumberAux: this.village || '',
        /* Tarjeta de crédito */
        issuingBankCode: this.datosPago.bancoEmisor.id,
        creditCardNumber: this.datosPago.numeroTarjeta,
        expirationMonth: this.datosPago.mesExpiracion,
        expirationYear: this.datosPago.anioExpiracion,
        /* Plan */
        plan: this.selectedPlan.replace('plan', ''),
        currentUF: this.valorUF,
      };
      return payload;
    },
    sendContractSigned() {
      this.TOGGLE_LOADER();
      this.scrollTop();
      const payload = this.setContractSignedPayload();
      this.SEND_CONTRACT_SIGNED(payload)
        .then(() => {
          this.sendProposal();
        }).catch(e => console.error(e));
    },
    sendProposal() {
      const payload = this.setProposalPayload();
      this.SEND_PROPOSAL(payload)
        .then((res) => {
          if (res.status !== 200) {
            this.TOGGLE_LOADER();
            this.showError(2);
          } else {
            this.TOGGLE_LOADER();

            const { email } = this.contactInfo;
            const { proposalNumber } = res.data;
            this.RESET_STATE();

            this.SET_VALUE({ name: 'email', value: email });
            this.SET_VALUE({ name: 'insuranceProposal', value: proposalNumber });

            this.continueTo('Exito');
          }
        });
    },
  },
};
