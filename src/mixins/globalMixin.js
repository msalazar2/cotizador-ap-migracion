import { mapState, mapMutations } from 'vuex';

export default {
  data: () => ({
    valueof: '',
  }),
  computed: {
    ...mapState(['isLoading']),
    isExito() {
      return this.$route.path === '/exito';
    },
  },
  methods: {
    ...mapMutations(['TOGGLE_LOADER', 'SET_VALUE', 'TOGGLE_ERROR', 'SET_ACTIVE_STEP', 'SET_PREV_PATH']),
    ...mapMutations('modals', ['SET_TOGGLE_MODAL']),
    showError(idError) {
      this.$router.push({
        name: 'Error',
        params: { idError },
      });
    },
    scrollTop() {
      window.scrollTo(0, 0);
    },
    continueTo(path) {
      this.$router.push({
        name: path,
      });
    },
    isSafari() {
      const agent = navigator.userAgent.toLowerCase();
      return (
        (agent.includes('iphone') || agent.includes('macintosh')) &&
                !agent.includes('chrome')
      );
    },
    sendTag(name, vpv) {
      if (vpv) {
        window.GAEventSender(name, `accidentes_flujo_${vpv}`);
      } else {
        window.GAEventSender(`accidentes_flujo_${name}`);
      }
    },
    toggle(name) {
      // if (!this[name]) {
      //   Envía evento Google Tag Manager
      //   GAEventSender(`onco_flujo_${name}`);
      // }
      this.SET_TOGGLE_MODAL(name);
    },
    goToUrl(url) {
      window.location.href = this[url];
    },
    handlers(map, vm) {
      const jsonR = { ...map };
      const key = (e) => {
        if (e.key && vm.search.length > 0) {
          this.valueof = vm.search;
        }
      };
      for (let index = 65; index < 91; index++) {
        jsonR[index] = key;
      }
      return jsonR;
    },
    setValue(value) {
      if (!value && !this.valueof) return '';
      // const regx = `/${this.valueof}/gi`
      const rg = new RegExp(this.valueof, 'gi');
      const strongVal = `<strong>${this.valueof}</strong>`;
      // strongVal += value.slice(this.valueof.length);
      return value.replace(rg, strongVal);
    },
    goHome() {
      window.location.href = 'https://www.consorcio.cl/seguros';
    },
  },

};
