import { mapState, mapGetters } from 'vuex';

export default {
  computed: {
    ...mapState(['client']),
    ...mapGetters(['valorUF']),
  },
  methods: {
    roundedNumberTreePlaces(num) {
      // return Math.round((Math.round(num) + Number.EPSILON) * 1000) / 1000;
      return Math.ceil(num / 100) * 100;
    },
    formatNumber(num) {
      return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1.');
    },
    convertUFToCLP(uf) {
      const num = this.actullyUFTtoClp(uf);
      const pesos = this.roundedNumberTreePlaces(num);
      return `${this.formatNumber(pesos)}`;
    },
    convertUFToCLPNonFormated(valorUf) {
      return this.actullyUFTtoClp(valorUf);
    },
    actullyUFTtoClp(uf) {
      if (uf && this.valorUF) {
        return uf * this.valorUF;
      }
      return `${0}`;
    },
    UfToString(uf) {
      return String(uf).replace('.', ',');
    },
  },
};
