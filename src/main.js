/* eslint import/no-unresolved: [0] */
/* eslint no-new: [0] */
import * as Sentry from '@sentry/browser';
import { Vue as VueIntegration } from '@sentry/integrations';
import VueSkeletonLoading from 'vue-skeleton-loading';
import VueCardFormat from 'vue-credit-card-validation';

import Vue from 'vue';
import App from './App';
import store from './store';
import router from './router';

/* Global mixins */
import globalMixin from './mixins/globalMixin';
import downloadFiles from './mixins/downloadFiles';
import formatMixin from './mixins/formatMixin';

window.GAEventSender = (name, vpv = '') => {
  if (typeof dataLayer !== 'undefined') {
    try {
      document.dispatchEvent(new CustomEvent(name));
      vpv
        ?
        dataLayer.push({ event: name, vpv }) :
        dataLayer.push({ event: name });
    } catch (error) {
      console.error(error);
    }
  }
};

Sentry.init({
  dsn: 'https://16416b17038a419b99ac1edbb99038fa@o346323.ingest.sentry.io/5209729',
  environment: window.liquid.sentryEnviroment,
  integrations: [new VueIntegration({ Vue, attachProps: true, logErrors: true })],
});

Vue.use(VueSkeletonLoading);
Vue.use(VueCardFormat);
Vue.mixin(globalMixin);
Vue.mixin(downloadFiles);
Vue.mixin(formatMixin);

new Vue({
  el: '#cotizador-ap',
  store,
  router,
  render: h => h(App),
});
