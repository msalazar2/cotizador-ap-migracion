const modals = {
  namespaced: true,
  state: {
    conoceDetalle: false,
    terminosInfoPersonal: false,
    modalMandatoPago: false,
    plan1500: false,
    plan1000: false,
    plan500: false,

  },
  mutations: {
    SET_TOGGLE_MODAL: (state, name) => {
      state[name] = !state[name];
    },
  },
};

export default modals;
