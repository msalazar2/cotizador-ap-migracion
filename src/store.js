/* eslint-disable no-empty-pattern */
/* eslint-disable no-empty */
import Vue from 'vue';
import Vuex from 'vuex';
import * as Sentry from '@sentry/browser';
/* Services */
import personService from './services/person';
import pricingService from './services/pricing';
import indicatorsService from './services/indicators';
import patService from './services/pat';
import geographyService from './services/geography';
import commonService from './services/personCommon';
import crossnetService from './services/crossnet';
import proposalService from './services/proposal';
/* Data */
import regions from './data/regions.json';
import planesInfo from './data/planesInfo.json';
/* Modules */
import modals from './modules/modals';

Vue.use(Vuex);

function testStorage() {
  try {
    // En algunos navegadores si se navega en modo incognito,
    // aparece activa la característica pero no deja accederla
    localStorage.setItem('1', '1');
    localStorage.removeItem('1');
    return true;
  } catch (e) {
    return false;
  }
}
// State inicial
const initialState = () => ({
  productName: 'Seguro Accidentes Personales',
  publicKey: window.liquid.captchaPublicKey,
  token_captcha: undefined,
  prevPath: '',
  activeStep: 1,
  errorPage: false,
  isLoading: false,
  // Paso 1
  dailyUFvalue: null,
  regions,
  nationalities: null,
  professions: null,
  rut: '',
  client: {},
  // Paso 1.2
  nationality: null,
  // Paso 1.3
  contactInfo: {
    email: null,
    telephone: null,
  },
  // Paso 2
  plans: {
    plan500: {
      name: 'plan500',
      title: 'Plan 500',
      feeCLP: '16856',
      feeUF: '0.59',
      modal: {
        name: 'plan500',
        alt: 'Indemnización',
        img:
          'https://consorcio.modyocdn.com/uploads/f73c94a6-e547-450f-b1a7-b97d28375809/original/landing_ap_user.svg',
        title: 'Plan 500',
        subtitle: 'Seguro Accidentes Personales Consorcio',
        content: {
          type: 'list-check',
          list: [
            'La cobertura de Muerte Accidental indemniza a tus beneficiarios con <strong> 500 UF </strong> si falleces a consecuencia de un accidente cubierto y en el plazo de 180 días de ocurrido éste.',
            ' La misma indemnización de <strong> 500 UF </strong> recibirás si pierdes, de manera irreversible y definitiva, al menos dos tercios de tu capacidad laboral producto de un accidente cubierto, dentro del plazo de 180 días de ocurrido éste.',
            'Considera reembolso de los gastos médicos, farmacéuticos y hospitalarios, que incurras en el plazo de 180 días ocurrido el accidente que los genere, hasta un monto máximo de <strong>100 UF</strong> en total. Se aplicará un deducible de <strong>2 UF</strong> por cada accidente que será siempre de tu costo. Debes utilizar previamente tu plan previsional de salud. ',
            'Si debes hospitalizarte producto de un accidente cubierto, contempla el pago de una indemnización de <strong>6 UF</strong> por cada día que permanezcas internado, a contar del  <strong> cuarto día</strong> y hasta máximo  <strong>10 días</strong>  de hospitalización. La carencia de días se aplicará para cada accidente cubierto que genere una hospitalización, pero no se aplicará para reingresos por el mismo accidente.',
          ],
        },
      },
    },
    plan1000: {
      name: 'plan1000',
      title: 'Plan 1000',
      feeCLP: '19147',
      feeUF: '0.67',
      modal: {
        name: 'plan1000',
        alt: 'Indemnización',
        img:
          'https://consorcio.modyocdn.com/uploads/f73c94a6-e547-450f-b1a7-b97d28375809/original/landing_ap_user.svg',
        title: 'Plan 1000',
        subtitle: 'Seguro Accidentes Personales Consorcio',
        content: {
          type: 'list-check',
          list: [
            'La cobertura de Muerte Accidental indemniza a tus beneficiarios con <strong>1.000 UF</strong> si falleces a consecuencia de un accidente cubierto y en el plazo de 180 días de ocurrido éste.',
            ' La misma indemnización de <strong>1.000 UF</strong> recibirás si pierdes, de manera irreversible y definitiva, al menos dos tercios de tu capacidad laboral producto de un accidente cubierto, dentro del plazo de 180 días de ocurrido éste.',
            'Considera reembolso de los gastos médicos, farmacéuticos y hospitalarios, que incurras en el plazo de 180 días ocurrido el accidente que los genere, hasta un monto máximo de <strong>100 UF</strong> en total. Se aplicará un deducible de <strong>2 UF</strong> por cada accidente que será siempre de tu costo. Debes utilizar previamente tu plan previsional de salud. ',
            'Si debes hospitalizarte producto de un accidente cubierto, contempla el pago de una indemnización de <strong>6 UF</strong> por cada día que permanezcas internado, a contar del  <strong> cuarto día</strong> y hasta máximo  <strong>10 días</strong>  de hospitalización. La carencia de días se aplicará para cada accidente cubierto que genere una hospitalización, pero no se aplicará para reingresos por el mismo accidente.',
          ],
        },
      },
    },
    plan1500: {
      name: 'plan1500',
      title: 'Plan 1500',
      feeCLP: '23131',
      feeUF: '0.81',
      modal: {
        name: 'plan1500',
        alt: 'Indemnización',
        img:
          'https://consorcio.modyocdn.com/uploads/f73c94a6-e547-450f-b1a7-b97d28375809/original/landing_ap_user.svg',
        title: 'Plan 1500',
        subtitle: 'Seguro Accidentes Personales Consorcio',
        content: {
          type: 'list-check',
          list: [
            'La cobertura de Muerte Accidental indemniza a tus beneficiarios con <strong>1000 UF</strong> si falleces a consecuencia de un accidente cubierto y en el plazo de 180 días de ocurrido éste.',
            ' La misma indemnización de <strong>1000 UF</strong> recibirás si pierdes, de manera irreversible y definitiva, al menos dos tercios de tu capacidad laboral producto de un accidente cubierto, dentro del plazo de 180 días de ocurrido éste.',
            'Considera reembolso de los gastos médicos, farmacéuticos y hospitalarios, que incurras en el plazo de 180 días ocurrido el accidente que los genere, hasta un monto máximo de <strong>100 UF</strong> en total. Se aplicará un deducible de <strong>2 UF</strong> por cada accidente que será siempre de tu costo. Debes utilizar previamente tu plan previsional de salud. ',
            'Si debes hospitalizarte producto de un accidente cubierto, contempla el pago de una indemnización de <strong>6 UF</strong> por cada día que permanezcas internado, a contar del  <strong> cuarto día</strong> y hasta máximo  <strong>10 días</strong>  de hospitalización. La carencia de días se aplicará para cada accidente cubierto que genere una hospitalización, pero no se aplicará para reingresos por el mismo accidente.',
          ],
        },
      },
    },
  },
  selectedPlan: null,
  primaMensualUF: null,
  primaMensualCLP: null,
  // Paso 3
  cities: [],
  districts: [],
  region: null,
  city: null,
  district: null,
  street: null,
  streetNumber: null,
  village: null,
  // Paso 3.1
  profession: null,
  // Paso 4
  datosPago: {
    numeroTarjeta: null,
    fechaExpiracion: null,
    mesExpiracion: null,
    anioExpiracion: null,
    bancoEmisor: null,
    tipoTarjeta: null,
    srcImgTipoTarjeta: null,
  },
  // Paso 5
  planesInfo,
  downloadTermsIsOK: false,
  authorizations: {
    aceptaUsoInfoPersonal: false,
    aceptaTerminosYPropuesta: false,
    aceptaTerminosYPat: false,
  },
  // Exito
  email: null,
  insuranceProposal: null,
  purchaseCompleted: false,
});

export default new Vuex.Store({
  state: initialState(),
  getters: {
    userName: (state) => {
      const firtsName = state.client.fullName.split(' ')[0];
      return `${firtsName} ${state.client.lastName}`.toLowerCase();
    },
    regions: state =>
      state.regions.map(item => ({
        glsRegion: item.glsRegion.toLowerCase(),
        codRegion: item.codRegion,
      })),
    cities: state =>
      state.cities.map(item => ({
        glsCiudad: item.glsCiudad.toLowerCase(),
        codCiudad: item.codCiudad,
      })),
    districts: state =>
      state.districts.map(item => ({
        glsComuna: item.glsComuna.toLowerCase(),
        codComuna: item.codComuna,
      })),
    name: (state) => {
      if (!state.client) {
        return '';
      }
      const lower = state.client.fullName.split(' ')[0].toLowerCase();
      const nameCapitalized = lower.replace(/^\w/, c => c.toUpperCase());
      return nameCapitalized;
    },
    valorUF: (state) => {
      if (state.dailyUFvalue) {
        return parseFloat(state.dailyUFvalue.replace(/[,$ ]+/g, ''));
      } else if (localStorage.dailyUFvalue) {
        return parseFloat(localStorage.dailyUFvalue.replace(/[,$ ]+/g, ''));
      }
      return 0;
    },
  },
  mutations: {
    SET_VALUE: (state, { name, value, param = null }) => {
      if (param) {
        state[name][param] = value;
      } else {
        state[name] = value;
      }
    },
    SET_ACTIVE_STEP: (state, step) => {
      state.activeStep = step;
    },
    TOGGLE_ERROR: (state) => {
      state.errorPage = !state.errorPage;
    },
    TOGGLE_LOADER: (state) => {
      state.isLoading = !state.isLoading;
    },
    // Reset state
    RESET_STATE: (state) => {
      Object.assign(state, initialState());
    },
    SET_PREV_PATH: (state, path) => {
      state.prevPath = path;
    },
    TOGGLE_CUSTOM_HEADER: (state, hasCustomHeader) => {
      state.customHeader = hasCustomHeader;
    },
    SET_CUSTOM_HEADER: (state, html) => {
      state.customHeaderContent = html;
    },
    SET_VALUE_PLAN: (state, { name, value }) => {
      state.plans[name].feeUF = value;
    },
  },
  actions: {
    GET_PERSON: ({ state, commit }, rut) =>
      personService
        .getPerson(rut, state.token_captcha)
        .then((data) => {
          const { frontPersonalAccidentsClientData } = data.data;
          commit('SET_VALUE', {
            name: 'client',
            value: frontPersonalAccidentsClientData.fullClientResponse,
          });
          return data.status;
        })
        .catch((e) => {
          console.error(e, 'ERROR - getPerson');
          Sentry.captureException(e);
          return e;
        }),
    GET_PRICING: ({ commit }) =>
      pricingService
        .getPricing()
        .then((data) => {
          const { planA, planB, planC } = data.data;
          commit('SET_VALUE_PLAN', { name: 'plan500', value: planA });
          commit('SET_VALUE_PLAN', { name: 'plan1000', value: planB });
          commit('SET_VALUE_PLAN', { name: 'plan1500', value: planC });
          return data.status;
        })
        .catch((e) => {
          console.error(e, 'ERROR - getPricing');
          Sentry.captureException(e);
          return e;
        }),
    GET_INDICATORS: ({ commit }) =>
      indicatorsService
        .getIndicators()
        .then((data) => {
          localStorage.dailyUFvalue = data.data.data.valor_uf;
          commit('SET_VALUE', {
            name: 'dailyUFvalue',
            value: data.data.data.valor_uf,
          });
          return data.status;
        })
        .catch((e) => {
          console.error(e, 'ERROR - getIndicators');
          Sentry.captureException(e);
          return e;
        }),
    GET_CITIES: ({ commit }, id) =>
      geographyService
        .getCity(id)
        .then(({ data }) => {
          const name = 'cities';
          commit('SET_VALUE', { name, value: data.data.ciudadVO });
          return true;
        })
        .catch((e) => {
          console.error(e, 'ERROR - getCities');
          Sentry.captureException(e);
          return false;
        }),
    GET_DISTRICTS: ({ commit }, id) =>
      geographyService
        .getDistrict(id)
        .then(({ data }) => {
          const name = 'districts';
          commit('SET_VALUE', { name, value: data.data.comunaVO });
          return true;
        })
        .catch((e) => {
          console.error(e, 'ERROR - getDistricts');
          Sentry.captureException(e);
          return false;
        }),
    SEND_CONTACT_INFORMATION: ({}, payload) =>
      crossnetService
        .sendContactInformation(payload)
        .then(({ status }) => status === 201)
        .catch((e) => {
          console.error(e, 'ERROR - sendContactInformation');
          Sentry.captureException(e);
          return false;
        }),
    SEND_CONTRACT_SIGNED: ({}, payload) =>
      crossnetService
        .sendContractSigned(payload)
        .then(({ status }) => status === 201)
        .catch((e) => {
          console.error(e, 'ERROR - sendContractSigned');
          Sentry.captureException(e);
          return false;
        }),
    SEND_PROPOSAL: ({}, payload) =>
      proposalService
        .sendProposal(payload)
        .then(res => res)
        .catch((e) => {
          console.error(e, 'ERROR - sendProposal');
          Sentry.captureException(e);
          return e.response.status;
        }),
    GET_BANK_LIST() {
      return new Promise((resolve, reject) => {
        let listaCache = [];
        try {
          listaCache = JSON.parse(atob(sessionStorage.getItem('blist') || btoa('[]')));
        } catch (exc) {}
        if (
          testStorage() &&
          listaCache &&
          Array.isArray(listaCache) &&
          listaCache.length > 0
        ) {
          resolve(listaCache);
        } else {
          sessionStorage.removeItem('blist');
          patService
            .getBankList()
            .then((result) => {
              sessionStorage.setItem('blist', btoa(JSON.stringify(result)));
              resolve(result);
            })
            .catch((e) => {
              console.error(e, 'ERROR - patService');
              Sentry.captureException(e);
              reject(e);
            });
        }
      });
    },
    GET_PROFESSIONS: ({ commit }) =>
      commonService
        .getProfessions()
        .then((data) => {
          commit('SET_VALUE', {
            name: 'professions',
            value: data.data.listado,
          });
          return data.status;
        })
        .catch((e) => {
          Sentry.captureException(e);
          console.error(e, 'ERROR - getProfessions');
          return e;
        }),
    GET_NATIONALITIES: ({ commit }) =>
      commonService
        .getNationalities()
        .then((data) => {
          commit('SET_VALUE', {
            name: 'nationalities',
            value: data.data.listado,
          });
          return data.status;
        })
        .catch((e) => {
          Sentry.captureException(e);
          console.error(e, 'ERROR - getNationalities');
          return e;
        }),
  },
  modules: {
    modals,
  },
});
