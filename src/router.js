import store from './store';
import VueRouter from 'vue-router'
/* Vistas */
/* Paso 1 */
import Paso1 from './views/paso1/Paso-1';
import Paso1A from './views/paso1/Paso-1.1';
import Paso1B from './views/paso1/Paso-1.2';
import Paso1C from './views/paso1/Paso-1.3';
/* Paso 2 */
import Paso2 from './views/Paso-2';
/* Paso 3 */
import Paso3 from './views/paso3/Paso-3';
import Paso3A from './views/paso3/Paso-3.1';
/* Paso 4 */
import Paso4 from './views/Paso-4';
/* Paso 5 */
import Paso5 from './views/Paso-5';
/* Éxito */
import Exito from './views/Exito';
/* Error */
import ErrorComponent from './components/Error';

const router = new VueRouter({
  routes: [
    {
      path: '/',
      redirect: '/paso-1',
    },
    /* Paso 1: Rut */
    {
      path: '/paso-1',
      component: Paso1,
      name: 'Paso1',
    },
    /* Paso 1.1: Confirma Rut */
    {
      path: '/paso-1/1',
      component: Paso1A,
      name: 'Paso1A',
      meta: { requiresClient: true },
    },
    /* Paso 1.2: Nacionalidad */
    {
      path: '/paso-1/2',
      component: Paso1B,
      name: 'Paso1B',
      meta: { requiresClient: true },
    },
    /* Paso 1.3: Datos de Contacto */
    {
      path: '/paso-1/3',
      component: Paso1C,
      name: 'Paso1C',
      meta: {
        requiresClient: true,
        requiresNationality: true,
      },
    },
    /* Paso 2: Planes */
    {
      path: '/paso-2',
      component: Paso2,
      name: 'Paso2',
      meta: {
        requiresClient: true,
        requiresNationality: true,
        requiresContactInfo: true,
      },
    },
    /* Paso 3: Dirección */
    {
      path: '/paso-3',
      component: Paso3,
      name: 'Paso3',
      meta: {
        requiresClient: true,
        requiresNationality: true,
        requiresContactInfo: true,
        requiresPrimaMensual: true,
      },
    },
    /* Paso 3.1: Profesión */
    {
      path: '/paso-3/1',
      component: Paso3A,
      name: 'Paso3A',
      meta: {
        requiresClient: true,
        requiresNationality: true,
        requiresContactInfo: true,
        requiresPrimaMensual: true,
        requiresInfoGeografica: true,
      },
    },
    /* Paso 4: Mandato de Pago */
    {
      path: '/paso-4',
      component: Paso4,
      name: 'Paso4',
      meta: {
        requiresClient: true,
        requiresNationality: true,
        requiresContactInfo: true,
        requiresPrimaMensual: true,
        requiresInfoGeografica: true,
        requiresProfession: true,
      },
    },
    /* Paso 5: Resumen Propuesta */
    {
      path: '/paso-5',
      component: Paso5,
      name: 'Paso5',
      meta: {
        requiresClient: true,
        requiresNationality: true,
        requiresContactInfo: true,
        requiresPrimaMensual: true,
        requiresInfoGeografica: true,
        requiresProfession: true,
        requiresPAT: true,
      },
    },
    /* Exito */
    {
      path: '/exito',
      component: Exito,
      name: 'Exito',
      meta: {
        requiresProposal: true,
      },
    },
    /* Utils */
    {
      path: '*',
      redirect: '/paso-1',
    },
    {
      path: '/:idError/error',
      component: ErrorComponent,
      name: 'Error',
    },
  ],
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(route => route.meta.requiresProposal)) {
    if (store.state.insuranceProposal && store.state.email) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresPAT)) {
    if (store.state.datosPago.numeroTarjeta && store.state.datosPago.fechaExpiracion && store.state.datosPago.bancoEmisor) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresProfession)) {
    if (store.state.profession) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresInfoGeografica)) {
    if (store.state.region && store.state.city && store.state.district) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresPrimaMensual)) {
    if (store.state.primaMensualUF && store.state.dailyUFvalue) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresContactInfo)) {
    if (store.state.contactInfo.email && store.state.contactInfo.telephone) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresNationality)) {
    if (store.state.nationality) {
      next();
    } else next('/paso-1');
  } else if (to.matched.some(route => route.meta.requiresClient)) {
    if (store.state.client.fullName) {
      next();
    } else next('/paso-1');
  } else next();
});
export default router;
